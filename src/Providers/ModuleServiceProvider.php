<?php

namespace Itec\Category\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Itec\Category\Models\CategoryType::class
    ];
}