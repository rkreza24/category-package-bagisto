<?php

namespace Itec\Category\Providers;

 use Illuminate\Support\ServiceProvider;
 use Illuminate\Support\Facades\Event;

 /**
 * HelloWorld service provider
 *
 * @author    Jane Doe <janedoe@gmail.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
 class CategoryServiceProvider extends ServiceProvider
 {
     /**
     * Bootstrap services.
     *
     * @return void
     */
     public function boot()
     {
        include __DIR__ . '/../Http/routes.php';

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'category_view');

        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'category_lang');

        $this->loadMigrationsFrom(__DIR__ .'/../Database/Migrations');

        Event::listen('bagisto.admin.layout.head', function($viewRenderEventManager) {
            $viewRenderEventManager->addTemplate('category_view::category.layouts.master');
        });

        $this->app->concord->registerModel(
            \Webkul\Category\Contracts\Category::class, \Itec\Category\Models\Category::class
        );

        $this->app->concord->registerModel(
            \Webkul\Category\Contracts\CategoryTranslation::class, \Itec\Category\Models\CategoryTranslation::class
        );

        // Event::listen('bagisto.admin.catalog.category.create_form_accordian.controls', function($viewRenderEventManager) {
        //    $viewRenderEventManager->addTemplate('category_view::category.category.create');
        // });

     }

     /**
     * Register services.
     *
     * @return void
     */
     public function register()
     {
          $this->mergeConfigFrom(dirname(__DIR__) . '/../Config/menu.php', 'menu.admin');

        //$this->mergeConfigFrom(dirname(__DIR__) . '/../Config/system.php', 'core');
     }
 }