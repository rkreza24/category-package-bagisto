 @extends('admin::layouts.content')

							<div class="control-group" :class="[errors.has('type_code') ? 'has-error' : '']">
                                <label for="type_code" class="required">{{ __('category_lang::app.categories.type') }}</label>
                                <select class="control" v-validate="'required'" id="type_code" name="type_code" data-vv-as="&quot;{{ __('category_lang::app.catalog.categories.type_code') }}&quot;">
                                    <option readonly disabled selected>
                                        {{ __('category_lang::app.categories.select_type') }}
                                    </option>
                                    @foreach ($category_type as $ct)
                                        <option value="{{ $ct->code }}"> {{ $ct->name}} </>
                                    @endforeach
                                </select>
                                <span class="control-error" v-if="errors.has('type_code')">@{{ errors.first('type_code') }}</span>
                            </div>