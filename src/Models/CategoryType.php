<?php

namespace Itec\Category\Models;

use Illuminate\Database\Eloquent\Model;
use Itec\Category\Contracts\CategoryType as CategoryTypeContract;

class CategoryType extends Model implements CategoryTypeContract
{

    protected $fillable = [
        'code',
        'name',
        'status',
    ];

}