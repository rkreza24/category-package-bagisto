<?php

//CategoryToute

Route::group(['middleware' => ['admin']], function () {

    //CategoryType Index
    Route::get('admin/catalog/category/type', 'Itec\Category\Http\Controllers\CategoryTypeController@index')
        ->defaults('_config', ['view' => 'category_view::category_type.index'])
        ->name('category.type.index');
    //CategoryType Create
    Route::get('admin/catalog/category/type/create', 'Itec\Category\Http\Controllers\CategoryTypeController@create')
        ->defaults('_config', ['view' => 'category_view::category_type.create'])
        ->name('category.type.create');


    //CategoryType Store
    Route::post('admin/catalog/category/type/store', 'Itec\Category\Http\Controllers\CategoryTypeController@store')
        ->defaults('_config', ['view' => 'category_view::category_type.index'])
        ->name('category.type.store');


    //CategoryType Edit
    Route::get('admin/catalog/category/type/edit/{id}', 'Itec\Category\Http\Controllers\CategoryTypeController@edit')
        ->defaults('_config', ['view' => 'category_view::category_type.edit'])
        ->name('category.type.edit');


    //CategoryType Update
     Route::put('admin/catalog/category/type/edit/{id}', 'Itec\Category\Http\Controllers\CategoryTypeController@update')
        ->defaults('_config', ['redirect' => 'category_view::category_type.index'])
        ->name('category.type.update');


    //CategoryType Delete
    Route::post('admin/catalog/category/type/delete/{id}', 'Itec\Category\Http\Controllers\CategoryTypeController@destroy')->name('category.type.delete');

});