<?php

return [
     [
        'key' => 'catalog.categories.type',
        'name' => 'category_lang::app.category_type.title',
        'route' => 'category.type.index',
        'sort' => 3,
        'icon-class' => '',
        'active' => true,
    ],
];